<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Site\ProfileController;
use App\Http\Controllers\Site\UserAlbumController;
use App\Http\Controllers\Site\SiteController;
use App\Http\Controllers\Board\BoardController;
use App\Http\Controllers\Board\AdminAuthController;
use App\Http\Controllers\Board\RoleController;
use App\Http\Controllers\Board\AdminController;
use App\Http\Controllers\Board\UserController;
use App\Http\Controllers\Board\AlbumController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


require __DIR__.'/auth.php';

Route::get('/' , [SiteController::class , 'index'] );
Route::get('/album/{album}' , [SiteController::class , 'album'] )->name('albums.show');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/profile' , [ProfileController::class , 'index'])->name('profile.show');
    Route::patch('/profile' , [ProfileController::class , 'update'])->name('profile.update');
    Route::patch('/password' , [ProfileController::class , 'update_password'])->name('password.update');
    Route::get('/albums' , [UserAlbumController::class , 'index'] )->name('user.albums.index');
    Route::get('/albums/create' , [UserAlbumController::class , 'create'] )->name('user.albums.create');
    Route::post('/albums' , [UserAlbumController::class , 'store'] )->name('user.albums.store');
    Route::get('/albums/{album}' , [UserAlbumController::class , 'show'] )->name('user.albums.show');
    Route::delete('/albums/{album}' , [UserAlbumController::class , 'destroy'] )->name('user.albums.destroy');
    Route::patch('/albums/{album}' , [UserAlbumController::class , 'update'] )->name('user.albums.update');

    Route::delete('/albums/{album}/images/{image}',[UserAlbumController::class , 'destroy_image'])->name('user.albums.images.destroy');
    Route::patch('/albums/{album}/images/{image}',[UserAlbumController::class , 'update_image'])->name('user.albums.images.update');
    Route::post('/albums/{album}/images',[UserAlbumController::class , 'store_images'])->name('user.albums.images.store');
});


Route::group(['prefix' => 'Board' , 'as' => 'board.' ], function() {
    Route::get('/login' , [AdminAuthController::class  , 'index'] );
    Route::post('/login' , [AdminAuthController::class  , 'login'] )->name('login');
    Route::group(['middleware' => 'admin'], function() {
        Route::get('/' , [BoardController::class  , 'index'] )->name('home');
        Route::resource('roles', RoleController::class);
        Route::resource('admins', AdminController::class);
        Route::get('/users' , [UserController::class , 'index' ] )->name('users.index');
        Route::get('/users/{user}' , [UserController::class , 'show' ] )->name('users.show');
        Route::get('/albums' , [AlbumController::class , 'index' ] )->name('albums.index');
        Route::get('/albums/{album}' , [AlbumController::class , 'show' ] )->name('albums.show');
        Route::delete('images/{image}' ,  [AlbumController::class , 'delete_image' ] )->name('images.destroy');
    });
});