<?php

namespace App\Policies;

use App\Models\Album;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Auth;
class AlbumPolicy
{
    use HandlesAuthorization;


    protected $permissions;


    public function __construct()
    {
        $this->permissions = Auth::guard('admin')->user()->role->permissions()->pluck('permission')->toArray();
    }


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny()
    {
        if (in_array('show-albums',$this->permissions)) 
            return true;

        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view()
    {
        if (in_array('show-album',$this->permissions)) 
            return true;

        return false;
    }

   


    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete()
    {
        if (in_array('delete-album',$this->permissions)) 
            return true;

        return false;
    }

   
}
