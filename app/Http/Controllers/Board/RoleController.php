<?php

namespace App\Http\Controllers\Board;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Http\Requests\Board\Roles\StoreRoleRequest;
use App\Http\Requests\Board\Roles\UpdateRoleRequest;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny',Role::class);

        return view('board.roles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create',Role::class);

        return view('board.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        $this->authorize('create',Role::class);

        $role = new Role;
        if(!$role->add($request))
            return back()->with('error' , 'Error , try again ' );

        return back()->with('success' , 'Role Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $this->authorize('view',Role::class);

        $role->load(['permissions' , 'admin']);
        return view('board.roles.show' , compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {

        $this->authorize('update',Role::class);

        $permissions = $role->permissions()->pluck('permission')->toArray();
        return view('board.roles.edit' , compact('role' , 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        $this->authorize('update',Role::class);

        if(!$role->edit($request))
            return back()->with('error' , 'Error , try again ' );

        return back()->with('success' , 'Role Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $this->authorize('delete',Role::class);

        if(!$role->remove())
            return back()->with('error' , 'Error , try again ' );

        return back()->with('success' , 'Role deleted successfully');
    }
}
