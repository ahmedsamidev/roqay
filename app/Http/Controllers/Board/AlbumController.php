<?php

namespace App\Http\Controllers\Board;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Album;
use App\Models\AlbumImage;
use Storage;
class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny',Album::class);
        return view('board.albums.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        $this->authorize('view',Album::class);
        $album->load('images');
        return view('board.albums.show' , compact('album') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete_image(AlbumImage $image)
    {
        $this->authorize('delete',Album::class);

        Storage::delete(['albums/'.$image->image]);
        $image->delete();
        return back()->with('success' , 'image deleted successfully');
    }
}
