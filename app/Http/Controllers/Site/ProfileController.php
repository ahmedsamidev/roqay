<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Hash;
use App\Http\Requests\Profile\UpdateProfileRequest;
use App\Http\Requests\Profile\UpdatePasswordRequest;



class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('site.user.profile' , compact('user'));
    }

    public function update(UpdateProfileRequest $request)
    {
        if (!Hash::check($request->password,Auth::user()->password )) {
            return back()->with('error' , 'Please put your your current valid password'  );
        }
        $user = Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        return back()->with('success'  , 'your profile information has been updated successfully' );
    }

    public function update_password(Request $request)
    {
        // dd($request->all());
        if (!Hash::check($request->current_password,Auth::user()->password )) {
            return back()->with('error' , 'Please put your your current valid password'  );
        }

        $user = Auth::user();
        $user->password = Hash::make($request->new_password);
        $user->save();
        return back()->with('success'  , 'your password has been updated successfully' );
    }


}
