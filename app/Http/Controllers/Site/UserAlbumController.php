<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Site\Albums\StoreAlbumRequest;
use Auth;
use Storage;
use App\Models\Album;
use App\Models\AlbumImage;
use App\Http\Requests\Site\Albums\StoreImageRequest;
class UserAlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::with('firstImage')->where('user_id' , Auth::id() )->get();

        return view('site.user.albums' , compact('albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('site.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAlbumRequest $request)
    {
        $album = new Album;
        $album->name = $request->name;
        $album->visibility =  $request->type;
        $album->user_id = Auth::id();
        $album->save();
        $images = [];
        for ($i = 0; $i <count($request->images) ; $i++) {
            $images[] = new AlbumImage([
                'image' => basename($request->file('images.'.$i)->store('album')) , 
                'visibility' => $request->type , 
                'album_id' => $album->id , 
            ]); 
        }

        $album->images()->saveMany($images);
        return back()->with('success'  , 'your album has been added successfully');
    }


    public function store_images(StoreImageRequest $request , Album $album)
    {
        $images = [];
        for ($i = 0; $i <count($request->images) ; $i++) {
            $images[] = new AlbumImage([
                'image' => basename($request->file('images.'.$i)->store('album')) , 
                'visibility' => 1 , 
                'album_id' => $album->id , 
            ]); 
        }

        $album->images()->saveMany($images);
        return back()->with('success'  , 'your images has been added successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        if($album->user_id != Auth::id() )
            return back()->with('error' , 'your don\'t have permission to see this album');

        $album->load('images');
        return view('site.user.album' , compact('album') );
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Album $album)
    {
        if($album->user_id != Auth::id() )
            return back()->with('error' , 'your don\'t have permission to update this album');

        $album->visibility = $request->visibility;
        $album->save();
        $album->images()->update([
            'visibility' => $request->visibility , 

        ]);


        return back()->with('success' , 'your album updated successfully');

    }


    public function update_image(Request $request,Album $album , AlbumImage $image )
    {
        // we need to check if this album related to this user or not
        if($album->user_id != Auth::id() )
            return back()->with('error' , 'your don\'t have permission to update this albut');

        // then we need to check if this image related to this album or not
        if($image->album_id != $album->id )
            return back()->with('error' , 'your don\'t have permission to update this image');

        $image->visibility = $request->visibility;
        $image->save();
        return back()->with('success' , 'your image updated successfully');
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Album $album)
    {
        if($album->user_id != Auth::id() )
            return back()->with('error' , 'your don\'t have permission to delete this album');
        $images = $album->images()->pluck('image')->toArray();
        foreach ($images as $image) {
            Storage::delete('album/'.$image);
        }
        $album->images()->delete();
        $album->delete();
        return back()->with('success' , 'album deleted successfully');

        
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_image(Album $album  ,AlbumImage $image)
    {
        // we need to check if this album related to this user or not
        if($album->user_id != Auth::id() )
            return back()->with('error' , 'your don\'t have permission to delete this albut');

        // then we need to check if this image related to this album or not
        if($image->album_id != $album->id )
            return back()->with('error' , 'we can not find this image in this album ');


        Storage::delete('album/'.$image->image);
        $image->delete();
        return back()->with('success' , 'image deleted successfully' );

    }




}
