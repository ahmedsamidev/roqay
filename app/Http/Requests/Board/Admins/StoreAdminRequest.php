<?php

namespace App\Http\Requests\Board\Admins;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required' , 
            'email' => 'required|email|unique:admins,email' , 
            'password' => 'required|confirmed' , 
            'role_id' => 'required', 
        ];
    }


    public function messages()
    {
        return [
            'role_id.required' => 'role is required' , 
        ];
    }
}
