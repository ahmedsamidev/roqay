<?php

namespace App\Http\Requests\Board\Roles;

use Illuminate\Foundation\Http\FormRequest;

class StoreRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:roles,name' , 
            'permissions' => 'required|array|min:1',
        ];
    }

    public function messages()
    {
        return [
            'permissions.min' => 'you must select at least one permission',
        ] ;
    }
}
