<?php

namespace App\Http\Requests\Site\Albums;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
class StoreImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'images.*' => 'image' ,
            'images' => 'required' , 
        ];
    }
}
