<?php

namespace App\Http\Requests\Site\Albums;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
class StoreAlbumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required' , 
            'images.*' => 'image' ,
            'images' => 'required' , 
            'type' => 'required' ,  
        ];
    }


    public function messages()
    {
        return [
            'images.*.image' => 'only images with png , jpg , jpeg , gif files allowd '
        ];
    }
}
