<?php

namespace App\Http\Livewire\Board\Albums;

use Livewire\Component;
use App\Models\Album;
use Livewire\WithPagination;
class ListAllAlbums extends Component
{
    use WithPagination;
    public $search = '';
    public $rows = 10;

    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingRows()
    {
        $this->resetPage();
    }

    public function render()
    {
        $albums = Album::query();
        if($this->search != '')
            $albums->where('name' ,'like', '%'.$this->search.'%' );

        $albums = $albums->latest()->paginate($this->rows);
        return view('livewire.board.albums.list-all-albums' , compact('albums'));
    }
}
