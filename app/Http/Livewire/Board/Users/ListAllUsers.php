<?php

namespace App\Http\Livewire\Board\Users;

use Livewire\Component;
use App\Models\User;
use Livewire\WithPagination;
class ListAllUsers extends Component
{
    use WithPagination;
    public $search = '';
    public $rows = 10;

    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingRows()
    {
        $this->resetPage();
    }

    public function render()
    {
        $users = User::query();
        if($this->search != '')
            $users->where('name' ,'like', '%'.$this->search.'%' )->orWhere('email' ,'like', '%'.$this->search.'%' );

        $users = $users->latest()->paginate($this->rows);
        return view('livewire.board.users.list-all-users' , compact('users'));
    }
}
