<?php

namespace App\Http\Livewire\Board\Roles;

use Livewire\Component;
use App\Models\Role;
use Livewire\WithPagination;
class ListAllRoles extends Component
{
    use WithPagination;
    public $search = '';
    public $rows = 10;

    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingRows()
    {
        $this->resetPage();
    }

    public function render()
    {
        $roles = Role::query()->with('admin');
        if($this->search != '')
            $roles->where('name' ,'like', '%'.$this->search.'%' );

        $roles = $roles->latest()->paginate($this->rows);
        return view('livewire.board.roles.list-all-roles' , compact('roles'));
    }
}
