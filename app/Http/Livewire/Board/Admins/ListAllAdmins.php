<?php

namespace App\Http\Livewire\Board\Admins;

use Livewire\Component;
use App\Models\Admin;
use Livewire\WithPagination;
class ListAllAdmins extends Component
{
    use WithPagination;
    public $search = '';
    public $rows = 10;

    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingRows()
    {
        $this->resetPage();
    }

    public function render()
    {
        $admins = Admin::query();
        if($this->search != '')
            $admins->where('name' ,'like', '%'.$this->search.'%' )->orWhere('email' ,'like', '%'.$this->search.'%' );

        $admins = $admins->latest()->paginate($this->rows);
        return view('livewire.board.admins.list-all-admins' , compact('admins'));
    }
}
