<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    use HasFactory;

    public function images()
    {
        return $this->hasMany(AlbumImage::class)->where('visibility' , 1 );
    }


    public function firstImage()
    {
        return $this->images()->take(1);
    }
}
