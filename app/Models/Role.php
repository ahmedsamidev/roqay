<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Models\RolePermission;
class Role extends Model
{
    use HasFactory;


    public function add($data)
    {
        $this->name = $data['name'];
        $this->admin_id = Auth::guard('admin')->id();
        $this->save();
        $permissions = [];

        foreach ($data['permissions'] as $permission) {
            $permissions[] = new RolePermission([
                'role_id' => $this->id , 
                'permission' => $permission , 
            ]);
        }
        $this->permissions()->saveMany($permissions);
        return true;
    }


    public function edit($data)
    {
        $this->name = $data['name'];
        $this->save();
        $this->permissions()->delete();
        $permissions = [];

        foreach ($data['permissions'] as $permission) {
            $permissions[] = new RolePermission([
                'role_id' => $this->id , 
                'permission' => $permission , 
            ]);
        }
        $this->permissions()->saveMany($permissions);
        return true;
    }



    public function permissions()
    {
        return $this->hasMany(RolePermission::class);
    }
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function remove()
    {
        $this->permissions()->delete();
        return $this->delete();
    }
}
