<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Hash;
class Admin extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;


    public function add($data)
    {
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->role_id = $data['role_id'];
        $this->password = Hash::make($data['password']);
        return $this->save();
    }
    public function edit($data)
    {
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->role_id = $data['role_id'];
        isset($data['password']) ?  Hash::make($data['password']) : $this->password;
        return $this->save();
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function remove()
    {
        return $this->delete();
    }

}
