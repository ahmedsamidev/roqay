@extends('site.layouts.master')

@section('page_title'  , 'Home' )

@section('content')
<section class="check_demo_movie">
	<div class="container">
		<h2 class=" wow fadeInDown"> <span class="main-color"> {{ $album->name }} </span></h2>

		@include('site.layouts.messages')

		<div class="row">
			
			@foreach ($album->images as $image)
			<div class="col-md-4">
				<div class="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
					<div class="card-header">
						<img src="{{ Storage::url('album/'.$image->image) }}" src="{{ Storage::url('album/'.$image->image) }}" class="lazyload">
					</div>

				</div>
			</div>
			@endforeach

		</div>
	</div>
</section>
@endsection