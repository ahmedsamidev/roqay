<!DOCTYPE html>
<html>

<head>
    <title> Smart Movies | @yield('page_title') </title>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="author" content="RoQaY">
    <meta name="robots" content="index, follow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" Smart Movies website">
    <meta name="keywords" content=" Smart Movies ">
    <meta name="csrf-token" content="V2G8zLS7dL5HzdfwxaBDewvJvAKCyeThQE4NBtJv">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('site_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('site_assets/css/fontawesome.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('site_assets/css/animate.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('site_assets/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('site_assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('site_assets/css/responsive.css') }}">
</head>

<body>
    <div class="body_wrapper">
        <div class="preloader">
            <div class="preloader-loading">
                <img src="{{ asset('site_assets/images/logo-m.png') }}" data-src="{{ asset('site_assets/images/logo-m.png') }}" class="lazyload">
            </div>
        </div>
        @include('site.layouts.header')
        @yield('content')
        @include('site.layouts.footer')
        <span class="scroll-top"> <a href="#"><i class="fas fa-chevron-up"></i></a> </span>
    </div>
    <script src="{{ asset('site_assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('site_assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('site_assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('site_assets/js/lazysizes.min.js') }}"></script>
    <script src="{{ asset('site_assets/js/fontawesome.min.js') }}"></script>
    <script src="{{ asset('site_assets/js/all.min.js') }}"></script>
    <script src="{{ asset('site_assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('site_assets/js/main.js') }}"></script>
</body>

</html>