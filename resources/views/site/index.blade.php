@extends('site.layouts.master')

@section('page_title'  , 'Home' )

@section('content')
<section class="check_demo_movie">
	<div class="container">
		<h2 class=" wow fadeInDown">Check Our <span class="main-color"> Packages</span></h2>
		
		<div class="row">
			@if (count($albums))
			@foreach ($albums as $album)
			<div class="col-md-4">
				<div class="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
					<div class="card-header">
						<img src="{{ Storage::url('album/'.optional($album->firstImage()->first())->image) }}" src="{{ Storage::url('album/'.optional($album->firstImage()->first())->image) }}" class="lazyload">
					</div>
					<div class="card-body">
						<h4> <a href="{{ route('albums.show' , ['album' => $album->id ] ) }}"> {{ $album->name }} </a></h4>
					</div>
				</div>
			</div>
			@endforeach
			@else
			<p> there is nor albus yet ,  </p>
			@endif
		</div>
	</div>
</section>
@endsection