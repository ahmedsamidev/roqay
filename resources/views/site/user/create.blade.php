@extends('site.layouts.master')

@section('page_title'  , 'Register' )

@section('content')
<section class="contact-us bg-light">
    <div class="container">
        <h3 class="text-center"> Create new album  </h3>
        @include('site.layouts.messages')
        <div class="row justify-content-center">
            <div class="col-md-7 col-sm-10">
                <div class="contact-form">
                    <form action="{{ route('user.albums.store') }}" method='POST' enctype="multipart/form-data" >
                        @csrf
                        <div class="form-group ">
                            <label for="inputName"> Album name </label>
                            <input type="text" name="name" id="inputName" class="form-control"
                            placeholder="Write Your Name">
                            @error('name')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputEmail"> album images  </label>
                            <input type="file" multiple='multiple' name="images[]" id="inputEmail" class="form-control">
                            @error('images.*')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label > album type </label>
                            <select name="type" id="inputType" class="form-control" required="required">
                                <option value="1">public</option>
                                <option value="0">private</option>
                            </select>
                            @error('type')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>

                        <div class="text-center p-2">
                            <button type="submit" class="btn btn-gradiant">
                                Add
                           </button>
                       </div>

 
                 </form>
             </div>
         </div>
     </div>
 </div>
</section>

@endsection