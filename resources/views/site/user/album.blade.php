@extends('site.layouts.master')

@section('page_title'  , 'Home' )

@section('content')
<section class="check_demo_movie">
	<div class="container">
		<h2 class=" wow fadeInDown"> <span class="main-color"> {{ $album->name }} </span></h2>
		<p>
			<label for=""> add more images </label>
			<form action="{{ route('user.albums.images.store'  , ['album' => $album->id ] ) }}" method='POST' enctype="multipart/form-data">
				@csrf
				<input type="file" multiple='multiple' onchange="form.submit()"  name="images[]"  >
				@error('images.*')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
			</form>
		</p>
		@include('site.layouts.messages')

		<div class="row">
			
			@foreach ($album->images as $image)
			<div class="col-md-4">
				<div class="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
					<div class="card-header">
						<img src="{{ Storage::url('album/'.$image->image) }}" src="{{ Storage::url('album/'.$image->image) }}" class="lazyload">
					</div>
					<div class="card-body">
						<ul>
							<li> 
								<form action="{{ route('user.albums.images.destroy' , [ 'album' => $album->id ,  'image' => $image->id ] ) }}" method='POST' >
									@csrf
									@method('DELETE')
									<button><span class='text-danger' > Delete </span> </button>
								</form> 
							</li>
							<li></li>
							<li>
								<form method='POST' action="{{ route('user.albums.images.update' , [ 'album' => $album->id  , 'image' => $image ] ) }}">
									@csrf
									@method('PATCH')
									<select name="visibility" onchange="this.form.submit()" id="input" class="form-control" required="required">
									<option value="1" {{ $image->visibility ==1 ? 'selected="selected"' : '' }} > public </option>
									<option value="0" {{ $image->visibility ==0 ? 'selected="selected"' : '' }} > private </option>
								</select>
								</form>
							</li>
						</ul>
					</div>
				</div>
			</div>
			@endforeach

		</div>
	</div>
</section>
@endsection