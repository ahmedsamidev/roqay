@extends('site.layouts.master')

@section('page_title'  , 'Register' )

@section('content')
<section class="contact-us bg-light">
    <div class="container">
                    @include('site.layouts.messages')

        <div class="row justify-content-center">

            <div class="col-md-6 col-sm-10">
                <div class="contact-form">
                    <h3 class="text-center">Edit Your Profile</h3>

                    <form action="{{ route('profile.update') }}" method='POST' >
                        @csrf
                        @method('PATCH')
                        <div class="form-group ">
                            <label for="inputName">Write Your Name</label>
                            <input type="text" name="name" value="{{ $user->name }}" id="inputName" class="form-control"
                            placeholder="Write Your Name">
                            @error('name')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Your Email Addrss</label>
                            <input type="email" name="email" value="{{ $user->email }}" id="inputEmail" class="form-control"
                            placeholder="Write Your Email">
                            @error('email')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputPassword"> Current password </label>
                            <input type="password" name='password' id="inputPassword" class="form-control" placeholder=" Write Your password">
                            @error('password')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>
                        <div class="text-center p-2">
                            <button type="submit" class="btn btn-gradiant">
                                Edit profile
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6 col-sm-10">
                <div class="contact-form">
                    <h3 class="text-center"> Edit Your Password </h3>

                    <form action="{{ route('password.update') }}" method='POST' >
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label for="inputPassword">Enter your new Password </label>
                            <input type="password" name='new_password' id="inputPassword" class="form-control" placeholder=" Write Your password">
                            @error('new_password')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputConfirmPassword">Confirm Password </label>
                            <input type="password" name='password_confirmation' id="inputConfirmPassword" class="form-control" placeholder="  Confirm Your password">
                            @error('password_confirmation')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputConfirmPassword"> Current Password </label>
                            <input type="password" name='current_password' id="inputConfirmPassword" class="form-control" placeholder="  Confirm Your password">
                            @error('current_password')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>


                        <div class="text-center p-2">
                            <button type="submit" class="btn btn-gradiant">
                                Change Password
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection