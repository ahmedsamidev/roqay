@extends('site.layouts.master')

@section('page_title'  , 'Home' )

@section('content')
<section class="check_demo_movie">
	<div class="container">
		<h2 class=" wow fadeInDown"> My <span class="main-color"> Albums </span></h2>
		<p> <a href="{{ route('user.albums.create') }}" class="btn btn-link btn-primary btn-lg" > add new album </a> </p>

		@include('site.layouts.messages')
		<div class="row">
			
			@if (count($albums))
			@foreach ($albums as $album)
			<div class="col-md-4">
				<div class="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
					<div class="card-header">
						<img src="{{ Storage::url('album/'.optional($album->firstImage()->first())->image) }}" src="{{ Storage::url('album/'.optional($album->firstImage()->first())->image) }}" class="lazyload">
					</div>
					<div class="card-body">
						<h4> <a href="{{ route('user.albums.show' , ['album' => $album->id ] ) }}"> {{ $album->name }} </a></h4>
						<ul>
							<li> 
								<form action="{{ route('user.albums.destroy' , [ 'album' => $album->id ] ) }}" method='POST' >
									@csrf
									@method('DELETE')
									<button><span class='text-danger' > Delete </span> </button>
								</form> 
							</li>
							<li>
								<form  method='POST' action="{{ route('user.albums.update' , [ 'album' => $album->id ] ) }}">
									@csrf
									@method('PATCH')
									<select name="visibility" onchange="this.form.submit()" id="input" class="form-control" required="required">
										<option value="1" {{ $album->visibility ==1 ? 'selected="selected"' : '' }} > public </option>
										<option value="0" {{ $album->visibility ==0 ? 'selected="selected"' : '' }} > private </option>
									</select>
								</form>
							</li>
						</ul>
					</div>
				</div>
			</div>
			@endforeach
			@else
			<p> there is nor albus yet ,  </p>
			@endif

		</div>
	</div>
</section>
@endsection