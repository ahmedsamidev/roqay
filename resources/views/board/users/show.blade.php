@extends('board.layouts.master')
@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">Users</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('board.home') }}">Home</a></li>
					<li class="breadcrumb-item"> <a href="{{ route('board.users.index') }}">Users</a> </li>
					<li class="breadcrumb-item active">Show User details</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@include('site.layouts.messages')
				<!-- general form elements -->
				<div class="card card-primary">
					<div class="card-header">
						<h3 class="card-title">Show User Details</h3>
					</div>
					<table class="table table-bordered table-hover">
						<tbody>
							<tr>
								<th> Date </th>
								<td> {{ $user->created_at->toDayDateTimeString() }} <span class='text-muted' > {{ $user->created_at->diffForHumans() }} </span> </td>
							</tr>
							<tr>
								<th> name</th>
								<td>{{ $user->name }}</td>
							</tr>
							<tr>
								<th> email </th>
								<td>{{ $user->email }}</td>
							</tr>
							<tr>
								<th> albums </th>
								<td>
									<ul>
										@foreach ($user->albums as $album)
											<li> <a href="{{ route('board.albums.show' , ['album' => $album->id ] ) }}"> {{ $album->name }} </a> </li>
										@endforeach
									</ul>
								</td>
							</tr>

						</tbody>
					</table>
					
				</div>
				<!-- /.card -->
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection