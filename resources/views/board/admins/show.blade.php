@extends('board.layouts.master')
@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">Admins</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('board.home') }}">Home</a></li>
					<li class="breadcrumb-item"> <a href="{{ route('board.admins.index') }}">Admins</a> </li>
					<li class="breadcrumb-item active">Show Admin details</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@include('site.layouts.messages')
				<!-- general form elements -->
				<div class="card card-primary">
					<div class="card-header">
						<h3 class="card-title">Show Admin Details</h3>
					</div>
					<table class="table table-bordered table-hover">
						<tbody>
							<tr>
								<th> Date </th>
								<td> {{ $admin->created_at->toDayDateTimeString() }} <span class='text-muted' > {{ $admin->created_at->diffForHumans() }} </span> </td>
							</tr>
							<tr>
								<th>Role</th>
								<td>{{ optional($admin->role)->name }}</td>
							</tr>
							<tr>
								<th> name</th>
								<td>{{ $admin->name }}</td>
							</tr>
							<tr>
								<th> email </th>
								<td>{{ $admin->email }}</td>
							</tr>

						</tbody>
					</table>
					
				</div>
				<!-- /.card -->
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection