@extends('board.layouts.master')
@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">Admins </h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('board.home') }}">Home</a></li>
					<li class="breadcrumb-item"><a href="{{ route('board.admins.index') }}">Admins</a></li>
					<li class="breadcrumb-item active">Add new Admin</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@include('site.layouts.messages')
				<!-- general form elements -->
				<div class="card card-primary">
					<div class="card-header">
						<h3 class="card-title">Add new Admin</h3>
					</div>
					<!-- /.card-header -->
					<!-- form start -->
					<form action="{{ route('board.admins.store') }}" method="POST" >
						@csrf
						<div class="card-body">
							<div class="form-group">
								<label for="exampleInputEmail1">name </label>
								<input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter  name ">
								@error('name')
								<p class='text-danger' >{{ $message }}</p>
								@enderror
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">email </label>
								<input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email ">
								@error('email')
								<p class='text-danger' >{{ $message }}</p>
								@enderror
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Password  </label>
								<input type="password" name="password" class="form-control" id="exampleInputEmail1" placeholder="Enter password ">
								@error('password')
								<p class='text-danger' >{{ $message }}</p>
								@enderror
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"> password confirmation </label>
								<input type="password" name="password_confirmation" class="form-control" id="exampleInputEmail1" placeholder="Enter password confirmation ">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"> Role </label>
								<select name="role_id" class="form-control" required="required">
									@foreach ($roles as $role)
										<option value="{{ $role->id }}"> {{ $role->name }} </option>
									@endforeach
								</select>
								@error('role_id')
								<p class='text-danger' >{{ $message }}</p>
								@enderror
							</div>
							
						</div>
						<!-- /.card-body -->

						<div class="card-footer">
							<button type="submit" class="btn btn-primary">Add </button>
						</div>
					</form>
				</div>
				<!-- /.card -->
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection