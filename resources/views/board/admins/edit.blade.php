@extends('board.layouts.master')
@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">Admins </h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('board.home') }}">Home</a></li>
					<li class="breadcrumb-item"><a href="{{ route('board.admins.index') }}">Admins</a></li>
					<li class="breadcrumb-item active">Edit  Admin</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@include('site.layouts.messages')
				<!-- general form elements -->
				<div class="card card-primary">
					<div class="card-header">
						<h3 class="card-title">Edit Admin</h3>
					</div>
					<!-- /.card-header -->
					<!-- form start -->
					<form action="{{ route('board.admins.update' , ['admin' => $admin->id] ) }}" method="POST" >
						@csrf
						@method('PATCH')
						<div class="card-body">
							<div class="form-group">
								<label for="exampleInputEmail1">name * </label>
								<input type="text" name="name"  value="{{ $admin->name }}" class="form-control" id="exampleInputEmail1" placeholder="Enter  name ">
								@error('name')
								<p class='text-danger' >{{ $message }}</p>
								@enderror
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">email *</label>
								<input type="email" name="email" value="{{ $admin->email }}"  class="form-control" id="exampleInputEmail1" placeholder="Enter email ">
								@error('email')
								<p class='text-danger' >{{ $message }}</p>
								@enderror
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">new Password  </label>
								<input type="password" name="password" class="form-control" id="exampleInputEmail1" placeholder="Enter password ">
								@error('password')
								<p class='text-danger' >{{ $message }}</p>
								@enderror
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"> new password confirmation </label>
								<input type="password" name="password_confirmation" class="form-control" id="exampleInputEmail1" placeholder="Enter password confirmation ">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"> Role *</label>
								<select name="role_id" class="form-control" required="required">
									@foreach ($roles as $role)
										<option value="{{ $role->id }}" {{ $admin->role_id == $role->id ? 'selected="selected"' : '' }} > {{ $role->name }} </option>
									@endforeach
								</select>
								@error('role_id')
								<p class='text-danger' >{{ $message }}</p>
								@enderror
							</div>
							
						</div>
						<!-- /.card-body -->

						<div class="card-footer">
							<button type="submit" class="btn btn-warning"> Edit </button>
						</div>
					</form>
				</div>
				<!-- /.card -->
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection