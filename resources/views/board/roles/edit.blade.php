@extends('board.layouts.master')
@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">Roles</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('board.home') }}">Home</a></li>
					<li class="breadcrumb-item">Roles</li>
					<li class="breadcrumb-item active">Edit Role</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@include('site.layouts.messages')
				<!-- general form elements -->
				<div class="card card-primary">
					<div class="card-header">
						<h3 class="card-title">Edit Role </h3>
					</div>
					<!-- /.card-header -->
					<!-- form start -->
					<form action="{{ route('board.roles.update' , ['role' => $role->id ] ) }}" method="POST" >
						@csrf
						@method('PATCH')
						<div class="card-body">
							<div class="form-group">
								<label for="exampleInputEmail1">Role name </label>
								<input type="text" name="name" value='{{ $role->name }}' class="form-control" id="exampleInputEmail1" placeholder="Enter role name ">
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]" {{ in_array('add-role' , $permissions ) ? 'checked' : '' }} value='add-role' class="form-check-input" id="exampleCheck1">
											<label class="form-check-label" for="exampleCheck1">Add new role</label>
										</div>
										@error('permissions')
										<p class="text-danger"> {{ $message }} </p>
										@enderror
									</div>
									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]" {{ in_array('delete-role' , $permissions ) ? 'checked' : '' }} value='delete-role' class="form-check-input" id="exampleCheck2">
											<label class="form-check-label" for="exampleCheck2">Delete Role </label>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]" {{ in_array('edit-role' , $permissions ) ? 'checked' : '' }} value='edit-role' class="form-check-input" id="exampleCheck3">
											<label class="form-check-label" for="exampleCheck3">Edit Role</label>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]" {{ in_array('show-role' , $permissions ) ? 'checked' : '' }} value='show-role' class="form-check-input" id="exampleCheck4">
											<label class="form-check-label" for="exampleCheck4">show  Role details</label>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]" {{ in_array('show-admins' , $permissions ) ? 'checked' : '' }} value='show-admins' class="form-check-input" id="exampleCheck6">
											<label class="form-check-label" for="exampleCheck6">show all Admins</label>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]"  {{ in_array('show-admin' , $permissions ) ? 'checked' : '' }} value='show-admin' class="form-check-input" id="exampleCheck7">
											<label class="form-check-label" for="exampleCheck7">show admin details</label>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]" {{ in_array('edit-admin' , $permissions ) ? 'checked' : '' }} value='edit-admin' class="form-check-input" id="exampleCheck5">
											<label class="form-check-label" for="exampleCheck5">
											Edit Admin</label>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]" {{ in_array('delete-admin' , $permissions ) ? 'checked' : '' }} value='delete-admin' class="form-check-input" id="exampleCheck8">
											<label class="form-check-label" for="exampleCheck8">delete admin</label>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]" {{ in_array('add-admin' , $permissions ) ? 'checked' : '' }} value='add-admin' class="form-check-input" id="exampleCheck9">
											<label class="form-check-label" for="exampleCheck9">add new admin </label>
										</div>
									</div>

									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]" {{ in_array('delete-album' , $permissions ) ? 'checked' : '' }}  value='delete-album' class="form-check-input" id="exampleCheck10">
											<label class="form-check-label" for="exampleCheck10">
											Delete Album</label>
										</div>
									</div>

									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]" {{ in_array('show-album' , $permissions ) ? 'checked' : '' }} value='show-album' class="form-check-input" id="exampleCheck11">
											<label class="form-check-label" for="exampleCheck11">
											Show Album</label>
										</div>
									</div>

									<div class="col-md-2">
										<div class="form-check">
											<input type="checkbox" name="permissions[]" {{ in_array('show-albums' , $permissions ) ? 'checked' : '' }}  value='show-albums' class="form-check-input" id="exampleCheck12">
											<label class="form-check-label" for="exampleCheck12">
											Show Albums</label>
										</div>
									</div>


								</div>
							</div>
						</div>
						<!-- /.card-body -->

						<div class="card-footer">
							<button type="submit" class="btn btn-warning"> edit </button>
						</div>
					</form>
				</div>
				<!-- /.card -->
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection