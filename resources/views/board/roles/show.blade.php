@extends('board.layouts.master')
@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">Roles</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('board.home') }}">Home</a></li>
					<li class="breadcrumb-item">Roles</li>
					<li class="breadcrumb-item active">Show role details</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@include('site.layouts.messages')
				<!-- general form elements -->
				<div class="card card-primary">
					<div class="card-header">
						<h3 class="card-title">Show Role Details</h3>
					</div>
					<table class="table table-bordered table-hover">
						<tbody>
							<tr>
								<th> Date </th>
								<td> {{ $role->created_at->toDayDateTimeString() }} <span class='text-muted' > {{ $role->created_at->diffForHumans() }} </span> </td>
							</tr>
							<tr>
								<th>adedd by</th>
								<td>{{ optional($role->admin)->name }}</td>
							</tr>
							<tr>
								<th>role name</th>
								<td>{{ $role->name }}</td>
							</tr>
							<tr>
								<th>role permissions</th>
								<td>
									<ul>
										@foreach ($role->permissions as $permission)
											<li> {{ $permission->permission }} </li>
										@endforeach
									</ul>
								</td>
							</tr>
						</tbody>
					</table>
					
				</div>
				<!-- /.card -->
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection