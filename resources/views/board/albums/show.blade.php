@extends('board.layouts.master')
@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">Albums </h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ route('board.home') }}">Home</a></li>
					<li class="breadcrumb-item"> <a href="{{ route('board.albums.index') }}">Albums</a> </li>
					<li class="breadcrumb-item active">{{ $album->name }}</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@include('site.layouts.messages')
				<div class="col-12">
					<div class="card card-primary">
						<div class="card-header">
							<h4 class="card-title"> {{ $album->name }} </h4>
						</div>
						<div class="card-body">
							<div>
								<div class="btn-group w-100 mb-2">
									<a class="btn btn-info active" href="javascript:void(0)" data-filter="all"> All items </a>
								</div>
							</div>
							<div>
								<div class="filter-container p-0 row">
									@foreach ($album->images as $image)
										<div class="filtr-item col-sm-2" data-category="1" data-sort="white sample">
										<a href="{{ Storage::url('album/'.$image->image) }}" data-toggle="lightbox" data-title="sample 1 - white">
											<img src="{{ Storage::url('album/'.$image->image) }}" class="img-fluid mb-2" alt="white sample"/>
										</a>
										<form method='POST' action="{{ route('board.images.destroy' , ['image' => $image->id ] ) }}">
											@csrf
											@method('DELETE')
											<button class='btn btn-sm btn-danger' > delete </button>
										</form>
									</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection



@section('styles')
<link rel="stylesheet" href="{{ asset('board_assets/plugins/ekko-lightbox/ekko-lightbox.css') }}">
@endsection


@section('scripts')

<script src="{{ asset('board_assets/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
<script>
	$(function () {
		$(document).on('click', '[data-toggle="lightbox"]', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox({
				alwaysShowClose: true
			});
		});

		$('.filter-container').filterizr({gutterPixels: 3});
		$('.btn[data-filter]').on('click', function() {
			$('.btn[data-filter]').removeClass('active');
			$(this).addClass('active');
		});
	})
</script>

@endsection