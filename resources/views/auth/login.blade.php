
@extends('site.layouts.master')

@section('page_title' , 'Login')

@section('content')
<section class="contact-us bg-light">
    <div class="container">
        <h3 class="text-center">Login To Join Us</h3>

        <div class="row justify-content-center">
            <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

            <div class="col-md-7 col-sm-10">
                <div class="contact-form">
                    <form action="{{ route('login') }}"  method='POST' >
                        @csrf
                        <div class="form-group">
                            <label for="inputEmail">Your Email Addrss</label>
                            <input type="email" name="email" id="inputEmail" class="form-control"
                            placeholder="Write Your Email">
                            @error('email')
                            <p class="text-danger"> {{ $message }} </p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputPassword">Your Password </label>
                            <input type="password" name="password" id="inputPassword" class="form-control" placeholder=" Write Your password">
                            @error('password')
                            <p class="text-danger"> {{ $message }} </p>
                            @enderror
                        </div>


                        <div class="text-center p-2">
                            <button type="submit" class="btn btn-gradiant">
                              login
                           </button>
                       </div>

                       <div >
                           <b> <span>Don't Have An Account ?</span> <a href="{{ route('register') }}" class="main-color ">Sign Up</a></b>
                       </div>
                   </form>
               </div>
           </div>
       </div>
   </div>
</section>

@endsection