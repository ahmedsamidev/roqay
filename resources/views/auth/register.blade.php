@extends('site.layouts.master')

@section('page_title'  , 'Register' )

@section('content')
<section class="contact-us bg-light">
    <div class="container">
        <h3 class="text-center">Sign Up To Join Us</h3>

        <div class="row justify-content-center">
            <div class="col-md-7 col-sm-10">
                <div class="contact-form">
                    <form action="{{ route('register') }}" method='POST' >
                        @csrf
                        <div class="form-group ">
                            <label for="inputName">Write Your Name</label>
                            <input type="text" name="name" id="inputName" class="form-control"
                            placeholder="Write Your Name">
                            @error('name')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Your Email Addrss</label>
                            <input type="email" name="email" id="inputEmail" class="form-control"
                            placeholder="Write Your Email">
                            @error('email')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputPassword">Enter Password </label>
                            <input type="password" name='password' id="inputPassword" class="form-control" placeholder=" Write Your password">
                            @error('password')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputConfirmPassword">Confirm Password </label>
                            <input type="password" name='password_confirmation' id="inputConfirmPassword" class="form-control" placeholder="  Confirm Your password">
                            @error('password_confirmation')
                            <p class='text-danger' > {{ $message }} </p>
                            @enderror
                        </div>

                        <div class="text-center p-2">
                            <button type="submit" class="btn btn-gradiant">
                                Sign Up
                           </button>
                       </div>

                       <div >
                         <b> <span>Have An Account ?</span> <a href="login.html" class="main-color ">Login</a></b>
                     </div>
                 </form>
             </div>
         </div>
     </div>
 </div>
</section>

@endsection