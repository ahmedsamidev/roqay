<div class="card">
    <div class="card-header bg-primary text-white">
        <h3 class="card-title">Show All admins</h3>

        <div class="card-tools">
            <div class="row">
                <div class="input-group input-group-sm" style="width: 300px;">
                    <input type="text" wire:model='search' class="form-control float-right" placeholder="Search admins">

                    <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
                <div class="input-group input-group-sm ml-2" style="width: 50px;">
                    <select wire:model='rows' id="input" class="form-control"  style="width: 50px;" required="required">
                        <option value="10">10 row</option>
                        <option value="20">20 row</option>
                        <option value="50">50 row</option>
                    </select>
                </div>
            </div>

        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>name</th>
                    <th> email </th>
                    <th> Role </th>
                    <th>Date</th>
                    <th>Added by </th>
                    <th> </th>
                </tr>
            </thead>
            <tbody>
                @php
                $i = 1;
                @endphp
                @foreach ($admins as $admin)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $admin->name }}</td>
                    <td>{{ $admin->email }}</td>
                    <td>{{ optional($admin->role)->name }}</td>
                    <td> {{ $admin->created_at->toDayDateTimeString() }} <span class="text-muted"> {{ $admin->created_at->diffForHumans() }} </span> </td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="{{ route('board.admins.show' , ['admin' => $admin->id] ) }}">
                          <i class="fas fa-folder">
                          </i>
                          View
                      </a>
                      <a class="btn btn-info btn-sm" href="{{ route('board.admins.edit' , ['admin' => $admin->id ] ) }}">
                          <i class="fas fa-pencil-alt">
                          </i>
                          Edit
                      </a>
                      <form method='POST' action="{{ route('board.admins.destroy' , ['admin' => $admin->id ] ) }}">
                          @csrf
                          @method('DELETE')
                         
                          <button  class="btn btn-danger btn-sm">
                               <i class="fas fa-trash">
                              </i>
                              Delete
                          </button>
                      </form>
                  </td>
              </tr>
              @endforeach
          </tbody>
      </table>
      {{ $admins->links() }}
  </div>

</div>