<div class="card">
    <div class="card-header bg-primary text-white">
        <h3 class="card-title">Show All users</h3>

        <div class="card-tools">
            <div class="row">
                <div class="input-group input-group-sm" style="width: 300px;">
                    <input type="text" wire:model='search' class="form-control float-right" placeholder="Search users">

                    <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
                <div class="input-group input-group-sm ml-2" style="width: 50px;">
                    <select wire:model='rows' id="input" class="form-control"  style="width: 50px;" required="required">
                        <option value="10">10 row</option>
                        <option value="20">20 row</option>
                        <option value="50">50 row</option>
                    </select>
                </div>
            </div>

        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>name</th>
                    <th> email </th>
                    <th>Date</th>
                    <th> </th>
                </tr>
            </thead>
            <tbody>
                @php
                $i = 1;
                @endphp
                @foreach ($users as $user)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td> {{ $user->created_at->toDayDateTimeString() }} <span class="text-muted"> {{ $user->created_at->diffForHumans() }} </span> </td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="{{ route('board.users.show' , ['user' => $user->id] ) }}">
                          <i class="fas fa-folder">
                          </i>
                          View
                      </a>
                  </td>
              </tr>
              @endforeach
          </tbody>
      </table>
      {{ $users->links() }}
  </div>

</div>