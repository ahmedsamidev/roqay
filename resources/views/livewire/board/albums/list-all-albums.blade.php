<div>
    <div class="card">
    <div class="card-header bg-primary text-white">
        <h3 class="card-title">Show All Albums</h3>

        <div class="card-tools">
            <div class="row">
                <div class="input-group input-group-sm" style="width: 300px;">
                    <input type="text" wire:model='search' class="form-control float-right" placeholder="Search albums">

                    <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
                <div class="input-group input-group-sm ml-2" style="width: 50px;">
                    <select wire:model='rows' id="input" class="form-control"  style="width: 50px;" required="required">
                        <option value="10">10 row</option>
                        <option value="20">20 row</option>
                        <option value="50">50 row</option>
                    </select>
                </div>
            </div>

        </div>
    </div>
    
</div>

@if (count($albums))
@foreach ($albums as $album)
<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            <img style="width:300px; height: 300px;" class='img-reponsive' src="{{ Storage::url('album/'.optional($album->firstImage()->first())->image) }}" >
        </div>
        <div class="card-body">
            <h4> <a href="{{ route('board.albums.show' , ['album' => $album->id ] ) }}"> {{ $album->name }} </a></h4>
        </div>
    </div>
</div>
@endforeach
@else
<p> there is nor albus yet ,  </p>
@endif
</div>